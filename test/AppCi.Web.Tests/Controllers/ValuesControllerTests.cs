using AppCi.Web.Controllers;
using MyTested.AspNetCore.Mvc;
using Xunit;

namespace AppCi.Web.Tests.Controllers
{
    public class ValuesControllerTests
    {
        [Fact]
        public void ValuesControllers_HasCorrectRoutes()
        {
            MyMvc.Routes()
                .ShouldMap("/api/values")
                .To<ValuesController>((controller) => controller.Index());
        }
    }
}