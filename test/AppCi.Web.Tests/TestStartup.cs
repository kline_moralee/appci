﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppCi.Web.Tests
{
    public class TestStartup : Startup
    {
        public void ConfigureTestServices(IServiceCollection services)
        {
            ConfigureServices(services);
        }
    }
}
