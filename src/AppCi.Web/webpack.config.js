const path = require('path');
const webpack = require('webpack');
const ForkCheckerPlugin = require('awesome-typescript-loader').ForkCheckerPlugin;
const resolveNgRoute = require('@angularclass/resolve-angular-routes');

function root(_path) {
  _path = _path || '.';
  return path.join(__dirname, _path);
}

function webpackConfig(options) {
  options = options || {};

  const CONSTANTS = {
    ENV: JSON.stringify(options.ENV),
    HMR: options.HMR
  };

  return {
    cache: true,

    devtool: 'source-map',

    resolve: {
      extensions: ['', '.ts', '.js', '.json']
    },

    entry: {
      polyfills: './ClientApp/polyfills.browser',
      vendor: './ClientApp/vendor.browser',
      main: './ClientApp/main.browser'
    },

    output: {
      path: root('wwwroot/dist'),
      filename: '[name].bundle.js',
      sourceMapFilename: '[name].map',
      chunkFilename: '[id].chunk.js'
    },

    module: {
      preLoaders: [
        // fix angular2
        {
          test: /(systemjs_component_resolver|system_js_ng_module_factory_loader)\.js$/,
          loader: 'string-replace',
          query: {
            search: '(lang_1(.*[\\n\\r]\\s*\\.|\\.))?(global(.*[\\n\\r]\\s*\\.|\\.))?(System|SystemJS)(.*[\\n\\r]\\s*\\.|\\.)import',
            replace: 'System.import',
            flags: 'g'
          }
        },
        {
          test: /.js$/,
          loader: 'string-replace',
          query: {
            search: 'moduleId: module.id,',
            replace: '',
            flags: 'g'
          }
        }
        // end angular2 fix
      ],
      loaders: [{
        test: /\.ts$/,
        loaders: [
          'awesome-typescript',
          '@angularclass/conventions-loader'
        ],
        exclude: [/\.(spec|e2e|d)\.ts$/]
      }, {
        test: /\.json$/,
        loader: 'json'
      }, {
        test: /\.html/,
        loader: 'raw'
      }, {
        test: /\.css$/,
        loader: 'raw'
      }]
    },

    plugins: [
      // fix angular2
      new webpack.ContextReplacementPlugin(
        /angular\/core\/src\/linker/,
        root('./ClientApp'),
        resolveNgRoute(root('./ClientApp'))
      ),
      // end angular2 fix
      new webpack.HotModuleReplacementPlugin(),
      new ForkCheckerPlugin(),
      new webpack.optimize.CommonsChunkPlugin({
        name: ['main', 'vendor', 'polyfills'],
        minChunks: Infinity
      }),
      new webpack.DefinePlugin(CONSTANTS),
      new webpack.ProgressPlugin({})
    ],

    node: {
      global: 'window',
      process: true,
      Buffer: false,
      crypto: 'empty',
      module: false,
      clearImmediate: false,
      setImmediate: false,
      clearTimeout: true,
      setTimeout: true
    }
  };
};

module.exports = webpackConfig;
