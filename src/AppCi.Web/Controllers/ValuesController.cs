using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace AppCi.Web.Controllers
{
    [Route("/api/values")]
    public class ValuesController : Controller
    {
        public ValuesController()
        {
        }

        [HttpGet]
        public IActionResult Index()
        {
            return Ok(new List<object>
            {
                new { Id = 1, Name = "Foo" },
                new { Id = 2, Name = "Bar" }
            });
        }
    }
}