import {NgModule} from '@angular/core';
import {RouterConfig, RouterModule} from '@angular/router';

import {Home, HomeModule} from './home';

const ROUTER_CONFIG: RouterConfig = [{
  path: '',
  pathMatch: 'full',
  component: Home
}, {
  path: 'about',
  loadChildren: './+about'
}];

@NgModule({
  declarations: [
    Home
  ],
  imports: [
    RouterModule.forChild(ROUTER_CONFIG),
    HomeModule
  ],
  providers: [],
})
export default class AppModule {
  static routes = ROUTER_CONFIG;
}
