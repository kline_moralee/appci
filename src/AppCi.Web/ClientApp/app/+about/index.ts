import {NgModule} from '@angular/core';
import {RouterConfig, RouterModule} from '@angular/router';

import {About} from './about';

export const ROUTER_CONFIG: RouterConfig = [{
  path: '',
  pathMatch: 'full',
  component: About
}];

@NgModule({
  declarations: [About],
  imports: [
    RouterModule.forChild(ROUTER_CONFIG)
  ]
})
export default class AboutModule {
  static routes = ROUTER_CONFIG;
}
