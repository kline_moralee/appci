import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {App} from './app/app';
import appModule from './app';

@NgModule({
  bootstrap: [
    App
  ],
  declarations: [
    App
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([], {
      useHash: true
    }),
    appModule
  ],
  providers: []
})
class MainModule { }

export function main() {
  return platformBrowserDynamic().bootstrapModule(MainModule);
}

export function bootstrapDomReady() {
  document.addEventListener('DOMContentLoaded', main);
}

if (ENV === 'development' && HMR) {
  if (document.readyState === 'complete') {
    main();
  } else {
    bootstrapDomReady();
  }

  module.hot.accept();
} else {
  bootstrapDomReady();
}